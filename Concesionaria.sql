USE [MyCompany]
GO
/****** Object:  Table [dbo].[AUTO1]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AUTO1](
	[id_auto] [int] NOT NULL,
	[patente] [nvarchar](50) NULL,
	[año] [datetime] NULL,
	[marca] [nvarchar](50) NULL,
	[modelo] [nvarchar](50) NULL,
	[precio] [int] NULL,
 CONSTRAINT [PK_AUTO] PRIMARY KEY CLUSTERED 
(
	[id_auto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CLIENTE]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CLIENTE](
	[Id_cli] [int] NOT NULL,
	[dni] [int] NULL,
	[nombre] [varchar](50) NULL,
	[apellido] [varchar](50) NULL,
 CONSTRAINT [PK_CLIENTE] PRIMARY KEY CLUSTERED 
(
	[Id_cli] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClienteAuto]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClienteAuto](
	[id_auto] [int] NOT NULL,
	[id_cli] [int] NOT NULL,
	[patente] [nvarchar](50) NULL,
	[Nombre] [nvarchar](50) NULL,
 CONSTRAINT [PK_ClienteAuto] PRIMARY KEY CLUSTERED 
(
	[id_auto] ASC,
	[id_cli] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClienteAuto]  WITH CHECK ADD  CONSTRAINT [FK_ClienteAuto_AUTO1] FOREIGN KEY([id_auto])
REFERENCES [dbo].[AUTO1] ([id_auto])
GO
ALTER TABLE [dbo].[ClienteAuto] CHECK CONSTRAINT [FK_ClienteAuto_AUTO1]
GO
ALTER TABLE [dbo].[ClienteAuto]  WITH CHECK ADD  CONSTRAINT [FK_ClienteAuto_CLIENTE] FOREIGN KEY([id_cli])
REFERENCES [dbo].[CLIENTE] ([Id_cli])
GO
ALTER TABLE [dbo].[ClienteAuto] CHECK CONSTRAINT [FK_ClienteAuto_CLIENTE]
GO
/****** Object:  StoredProcedure [dbo].[C_alta]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[C_alta]	
	 @dni int, @nom varchar (50), @ape varchar (50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	 

	declare @id int
	set @id = ISNULL( (select max (id_cli) from CLIENTE), 0) + 1

    -- Insert statements for procedure here
insert into  CLIENTE  values (@id, @dni, @nom, @ape )
END
GO
/****** Object:  StoredProcedure [dbo].[C_alta_auto]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[C_alta_auto]	
	 @pat varchar (50), @ano datetime, @mar varchar (50), @mod varchar (50), @pre int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	 

	declare @id int
	set @id = ISNULL( (select max (id_auto) from AUTO1), 0) + 1

    -- Insert statements for procedure here
insert into  AUTO1  values (@id, @pat, @ano, @mar,@mod,@pre )
END
GO
/****** Object:  StoredProcedure [dbo].[C_compra]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE	 PROCEDURE [dbo].[C_compra]	
	 @idcli int,  @idau int, @pat  nvarchar(50), @nom  nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	 
	  

    -- Insert statements for procedure here
insert into  ClienteAuto   (id_auto,id_cli,patente, nombre)
select  b.id_auto, a.Id_cli, b.patente,  a.nombre from
CLIENTE a inner join AUTO1  b
on a.Id_cli=@idcli and b.id_auto=@idau

 




END
GO
/****** Object:  StoredProcedure [dbo].[C_del]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[C_del]
	 @id int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	 

	delete from CLIENTE where id_cli=@id 

 

END
 
GO
/****** Object:  StoredProcedure [dbo].[C_del_auto]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[C_del_auto]
	 @id int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	 

	delete from AUTO1 where id_auto=@id 

 

END
 
GO
/****** Object:  StoredProcedure [dbo].[C_Listar]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[C_Listar] 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from CLIENTE
END
GO
/****** Object:  StoredProcedure [dbo].[C_Listar_auto]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[C_Listar_auto] 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from AUTO1
END
GO
/****** Object:  StoredProcedure [dbo].[C_Listar_clienteAuto]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[C_Listar_clienteAuto] 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from ClienteAuto
END
GO
/****** Object:  StoredProcedure [dbo].[C_Listar_venta]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[C_Listar_venta] 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
select  b.id_auto, a.Id_cli, b.patente,  a.nombre from
CLIENTE a inner join clienteAuto  b 
on a.Id_cli = b.id_cli
inner join auto1 v on
v.id_auto= b.id_auto

    -- Insert statements for procedure here
	

END
GO
/****** Object:  StoredProcedure [dbo].[C_MOD]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[C_MOD]
	 @id int, @dni int, @nom varchar (50), @ape varchar (50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	 update  cliente  set
	 dni=@dni,
	 nombre =@nom,
	 apellido=@ape
	  where id_cli=@id


END
GO
/****** Object:  StoredProcedure [dbo].[C_MOD_auto]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[C_MOD_auto]
	 @id int, @pat varchar (50), @ano datetime, @mar varchar (50), @mod varchar (50), @pre int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	 update  AUTO1  set
	 patente=@pat,
	 año =@ano,
	 marca=@mar,
	 modelo=@mod,
	 precio=@pre
	  where id_auto=@id


END
GO
/****** Object:  StoredProcedure [dbo].[P_Listar]    Script Date: 08/09/2020 09:29:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[P_Listar] 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from CLIENTE
END
GO
