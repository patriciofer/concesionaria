﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace Negocio
{
    internal class Acceso
    {
        private SqlConnection conexion;

        public void Abrir()
        {
            conexion = new SqlConnection("initial catalog=MyCompany; Data Source=PATITO-PC\\SQLEXPRESS; Integrated Security=SSPI");
            conexion.Open();
        }


        public void Cerrar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();

        }


        private SqlCommand CrearComando(string sql, List<SqlParameter> parametros = null, CommandType tipo = CommandType.Text)
        {
            SqlCommand comando = new SqlCommand(sql);
            comando.CommandType = tipo;

            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }

            comando.Connection = conexion;
            return comando;

        }


        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.String;
            return p;
        }


        public SqlParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Int32;
            return p;
        }


        public SqlParameter CrearParametro(string nombre, DateTime valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.DateTime;
            return p;
        }

        public DataTable Leer(string sql, List<SqlParameter> parameters = null)
        {
            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = CrearComando(sql, parameters);
            DataTable tabla = new DataTable();

            adaptador.Fill(tabla);

            return tabla;
        }

        public int Escribir(string sql, List<SqlParameter> parameters = null)
        {
            SqlCommand comando = CrearComando(sql, parameters, CommandType.StoredProcedure);
            int filasAfectadas = 0;
            try
            {
                filasAfectadas = comando.ExecuteNonQuery();
            }
            catch (Exception ex )
            {

                filasAfectadas = -1;
            }

            return filasAfectadas;
        }



    }
}
