﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;



namespace Negocio
{
    public class Auto
    {

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        private string patente;

        public string Patente
        {
            get { return patente; }
            set { patente = value; }
        }

        private DateTime año;

        public DateTime Año
        {
            get { return año; }
            set { año = value; }
        }


        private string marca;

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }


        private string  modelo;

        public string  Modelo
        {
            get { return modelo; }
            set { modelo = value; }
        }


        private int precio;

        public int Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        public override string ToString()
        {
            return patente.ToString();
        }


        public static List<Auto> autolistar()
        {
            List<Auto> listAuto = new List<Auto>();
            Acceso acceso = new Acceso();
            acceso.Abrir();

            DataTable tabla = acceso.Leer("C_listar_auto");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Auto au = new Auto();
                au.id = int.Parse(registro["id_auto"].ToString());
                au.patente = registro["patente"].ToString();
                au.año = DateTime.Parse(registro["año"].ToString());
                au.marca = registro["marca"].ToString();
                au.modelo = registro["modelo"].ToString();
                au.precio = int.Parse(registro["precio"].ToString());
                listAuto.Add(au);
            }




            return listAuto;
        }


        public static int Insertarauto(string pa, string an, string mar, string mo, string pre)

        {
            int r = 0;
            try
            {
                Acceso acceso = new Acceso();
                acceso.Abrir();



                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter pat = new SqlParameter();
                pat.ParameterName = "@pat";
                pat.Value = pa;
                pat.SqlDbType = SqlDbType.Text;
                parametros.Add(pat);

                SqlParameter ano = new SqlParameter();
                ano.ParameterName = "@ano";
                ano.Value = an;
                ano.SqlDbType = SqlDbType.DateTime;
                parametros.Add(ano);



                SqlParameter marc = new SqlParameter();
                marc.ParameterName = "@mar";
                marc.Value = mar;
                marc.SqlDbType = SqlDbType.Text;
                parametros.Add(marc);


                SqlParameter mode = new SqlParameter();
                mode.ParameterName = "@mod";
                mode.Value = mo;
                mode.SqlDbType = SqlDbType.Text;
                parametros.Add(mode);

                SqlParameter prec = new SqlParameter();
                prec.ParameterName = "@pre";
                prec.Value = pre;
                prec.SqlDbType = SqlDbType.Int;
                parametros.Add(prec);



                r = acceso.Escribir("c_alta_auto", parametros);
            }

            catch (Exception)
            {
                r = -1;
            }
            return r;


        }


        public static int updateauto(string i, string pa, string an, string mar, string mo, string pre)

        {
            int r = 0;
            try
            {
                Acceso acceso = new Acceso();
                acceso.Abrir();



                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter ida = new SqlParameter();
                ida.ParameterName = "@id";
                ida.Value = i;
                ida.SqlDbType = SqlDbType.Int;
                parametros.Add(ida);


                SqlParameter pat = new SqlParameter();
                pat.ParameterName = "@pat";
                pat.Value = pa;
                pat.SqlDbType = SqlDbType.Text;
                parametros.Add(pat);

                SqlParameter ano = new SqlParameter();
                ano.ParameterName = "@ano";
                ano.Value = an;
                ano.SqlDbType = SqlDbType.DateTime;
                parametros.Add(ano);



                SqlParameter marc = new SqlParameter();
                marc.ParameterName = "@mar";
                marc.Value = mar;
                marc.SqlDbType = SqlDbType.Text;
                parametros.Add(marc);


                SqlParameter mode = new SqlParameter();
                mode.ParameterName = "@mod";
                mode.Value = mo;
                mode.SqlDbType = SqlDbType.Text;
                parametros.Add(mode);

                SqlParameter prec = new SqlParameter();
                prec.ParameterName = "@pre";
                prec.Value = pre;
                prec.SqlDbType = SqlDbType.Int;
                parametros.Add(prec);



                r = acceso.Escribir("c_MOD_auto", parametros);
            }

            catch (Exception)
            {
                r = -1;
            }
            return r;


        }

        public static int deleteauto(string i )

        {
            int r = 0;
            try
            {
                Acceso acceso = new Acceso();
                acceso.Abrir();



                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter ida = new SqlParameter();
                ida.ParameterName = "@id";
                ida.Value = i;
                ida.SqlDbType = SqlDbType.Int;
                parametros.Add(ida);

                 


                r = acceso.Escribir("c_del_auto", parametros);
            }

            catch (Exception)
            {
                r = -1;
            }
            return r;


        }

    }



}
