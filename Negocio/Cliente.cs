﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace Negocio
{
    public class Cliente
    {

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }



        private int dni;

        public int Dni
        {
            get { return dni; }
            set { dni = value; }
        }


        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private Auto auto;

        public Auto Auto
        {
            get { return auto; }
            set { auto = value; }
        }

        public override string ToString()
        {
            return id.ToString();
        }


        public static List<Cliente> clientelistar()
        {
            List<Cliente> listcliente = new List<Cliente>();
            Acceso acceso = new Acceso();
            acceso.Abrir();

            DataTable tabla = acceso.Leer("C_listar");
            acceso.Cerrar();

            foreach (DataRow  registro  in tabla.Rows)
            {
                Cliente cli = new Cliente();
                cli.id = int.Parse(registro["id_cli"].ToString());
                cli.dni = int.Parse(registro["dni"].ToString());
                cli.nombre = registro["nombre"].ToString();
                cli.apellido = registro["apellido"].ToString();

                listcliente.Add(cli);
            }



          
            return listcliente;
        }

        public static List<ClienteAuto> Compralistar()
        {
            List<ClienteAuto> listclienteAuto = new List<ClienteAuto>();
            Acceso acceso = new Acceso();
            acceso.Abrir();

            DataTable tabla = acceso.Leer("C_Listar_venta");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Cliente cli = new Cliente();
                cli.id = int.Parse(registro["id_cli"].ToString());
                cli.nombre = registro["nombre"].ToString();

                Auto aut = new Auto();
                aut.Id = int.Parse(registro["id_auto"].ToString());
                aut.Patente = registro["patente"].ToString();

                ClienteAuto cliAuto = new ClienteAuto();
                cliAuto.Cliente = cli;
                cliAuto.Auto = aut;

                listclienteAuto.Add(cliAuto);


            }




            return listclienteAuto;
        }






        public static int InsertarCliente  (string doc, string nomb, string ape)

        {
            int r = 0;
            try
            {
                Acceso acceso = new Acceso();
            acceso.Abrir();



            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter dni = new SqlParameter();
            dni.ParameterName = "@dni";
            dni.Value = doc;
            dni.SqlDbType = SqlDbType.Int;
            parametros.Add(dni);

            SqlParameter nombre = new SqlParameter();
            nombre.ParameterName = "@nom";
            nombre.Value = nomb;
            nombre.SqlDbType = SqlDbType.Text;
            parametros.Add(nombre);



            SqlParameter apellido = new SqlParameter();
            apellido.ParameterName = "@ape";
            apellido.Value = ape;
            apellido.SqlDbType = SqlDbType.Text;
            parametros.Add(apellido);


          

             r= acceso.Escribir("c_alta", parametros);
            }

            catch (Exception)
            {
                r = -1;
            }
            return r;
          







        }


        public static int Comprar(string idcli, string idau, string nomb, string pa)

        {
            int r = 0;
            try
            {
                Acceso acceso = new Acceso();
                acceso.Abrir();



                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter idc = new SqlParameter();
                idc.ParameterName = "@idcli";
                idc.Value = idcli;
                idc.SqlDbType = SqlDbType.Int;
                parametros.Add(idc);

                SqlParameter ida = new SqlParameter();
                ida.ParameterName = "@idau";
                ida.Value = idau;
                ida.SqlDbType = SqlDbType.Int;
                parametros.Add(ida);


                SqlParameter pat = new SqlParameter();
                pat.ParameterName = "@pat";
                pat.Value = pa;
                pat.SqlDbType = SqlDbType.Text;
                parametros.Add(pat);

                SqlParameter nombre = new SqlParameter();
                nombre.ParameterName = "@nom";
                nombre.Value = nomb;
                nombre.SqlDbType = SqlDbType.Text;
                parametros.Add(nombre);







                r = acceso.Escribir("c_compra", parametros);
            }

            catch (Exception)
            {
                r = -1;
            }
            return r;








        }

        public static int UpdateCliente(string id, string doc, string nomb, string ape)

        {
            int r = 0;
            try
            {
                Acceso acceso = new Acceso();
                acceso.Abrir();



                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter i = new SqlParameter();
                i.ParameterName = "@id";
                i.Value = id;
                i.SqlDbType = SqlDbType.Int;
                parametros.Add(i);

                SqlParameter dni = new SqlParameter();
                dni.ParameterName = "@dni";
                dni.Value = doc;
                dni.SqlDbType = SqlDbType.Int;
                parametros.Add(dni);

                SqlParameter nombre = new SqlParameter();
                nombre.ParameterName = "@nom";
                nombre.Value = nomb;
                nombre.SqlDbType = SqlDbType.Text;
                parametros.Add(nombre);



                SqlParameter apellido = new SqlParameter();
                apellido.ParameterName = "@ape";
                apellido.Value = ape;
                apellido.SqlDbType = SqlDbType.Text;
                parametros.Add(apellido);




                r = acceso.Escribir("C_MOD", parametros);
            }

            catch (Exception)
            {
                r = -1;
            }
            return r;


        }


        public static int DeleteCliente(int id )

        {
            int r = 0;
            try
            {
                Acceso acceso = new Acceso();
                acceso.Abrir();



                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter i = new SqlParameter();
                i.ParameterName = "@id";
                i.Value = id;
                i.SqlDbType = SqlDbType.Int;
                parametros.Add(i);
 



                r = acceso.Escribir("C_del", parametros);
            }

            catch (Exception)
            {
                r = -1;
            }
            return r;


        }



    }


    }
 
