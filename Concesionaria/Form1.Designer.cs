﻿namespace Concesionaria
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrillaCliente = new System.Windows.Forms.DataGridView();
            this.txtdni = new System.Windows.Forms.TextBox();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.txtapellido = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnaltacli = new System.Windows.Forms.Button();
            this.btnbajacli = new System.Windows.Forms.Button();
            this.btnmodcli = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnaltaAuto = new System.Windows.Forms.Button();
            this.btnbajaauto = new System.Windows.Forms.Button();
            this.btnmodauto = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtmodelo = new System.Windows.Forms.TextBox();
            this.txtprecio = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bncomprar = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtpatente = new System.Windows.Forms.TextBox();
            this.txtaño = new System.Windows.Forms.TextBox();
            this.txtmarca = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.GrillaCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrillaCliente
            // 
            this.GrillaCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GrillaCliente.Location = new System.Drawing.Point(27, 115);
            this.GrillaCliente.Name = "GrillaCliente";
            this.GrillaCliente.Size = new System.Drawing.Size(375, 150);
            this.GrillaCliente.TabIndex = 0;
            // 
            // txtdni
            // 
            this.txtdni.Location = new System.Drawing.Point(131, 28);
            this.txtdni.Name = "txtdni";
            this.txtdni.Size = new System.Drawing.Size(100, 20);
            this.txtdni.TabIndex = 1;
            // 
            // txtnombre
            // 
            this.txtnombre.Location = new System.Drawing.Point(131, 54);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(100, 20);
            this.txtnombre.TabIndex = 2;
            // 
            // txtapellido
            // 
            this.txtapellido.Location = new System.Drawing.Point(131, 80);
            this.txtapellido.Name = "txtapellido";
            this.txtapellido.Size = new System.Drawing.Size(100, 20);
            this.txtapellido.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "DNI";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "NOMBRE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "APELLIDO";
            // 
            // btnaltacli
            // 
            this.btnaltacli.Location = new System.Drawing.Point(27, 272);
            this.btnaltacli.Name = "btnaltacli";
            this.btnaltacli.Size = new System.Drawing.Size(75, 23);
            this.btnaltacli.TabIndex = 7;
            this.btnaltacli.Text = "ALTA";
            this.btnaltacli.UseVisualStyleBackColor = true;
            this.btnaltacli.Click += new System.EventHandler(this.btnaltacli_Click);
            // 
            // btnbajacli
            // 
            this.btnbajacli.Location = new System.Drawing.Point(109, 272);
            this.btnbajacli.Name = "btnbajacli";
            this.btnbajacli.Size = new System.Drawing.Size(75, 23);
            this.btnbajacli.TabIndex = 8;
            this.btnbajacli.Text = "BAJA";
            this.btnbajacli.UseVisualStyleBackColor = true;
            this.btnbajacli.Click += new System.EventHandler(this.btnbajacli_Click);
            // 
            // btnmodcli
            // 
            this.btnmodcli.Location = new System.Drawing.Point(191, 272);
            this.btnmodcli.Name = "btnmodcli";
            this.btnmodcli.Size = new System.Drawing.Size(106, 23);
            this.btnmodcli.TabIndex = 9;
            this.btnmodcli.Text = "MODIFICACION";
            this.btnmodcli.UseVisualStyleBackColor = true;
            this.btnmodcli.Click += new System.EventHandler(this.btnmodcli_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(58, 109);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(306, 150);
            this.dataGridView1.TabIndex = 11;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(18, 29);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(549, 150);
            this.dataGridView2.TabIndex = 12;
            // 
            // btnaltaAuto
            // 
            this.btnaltaAuto.Location = new System.Drawing.Point(58, 266);
            this.btnaltaAuto.Name = "btnaltaAuto";
            this.btnaltaAuto.Size = new System.Drawing.Size(75, 23);
            this.btnaltaAuto.TabIndex = 13;
            this.btnaltaAuto.Text = "ALTA";
            this.btnaltaAuto.UseVisualStyleBackColor = true;
            this.btnaltaAuto.Click += new System.EventHandler(this.btnaltaAuto_Click);
            // 
            // btnbajaauto
            // 
            this.btnbajaauto.Location = new System.Drawing.Point(157, 266);
            this.btnbajaauto.Name = "btnbajaauto";
            this.btnbajaauto.Size = new System.Drawing.Size(75, 23);
            this.btnbajaauto.TabIndex = 14;
            this.btnbajaauto.Text = "BAJA";
            this.btnbajaauto.UseVisualStyleBackColor = true;
            this.btnbajaauto.Click += new System.EventHandler(this.btnbajaauto_Click);
            // 
            // btnmodauto
            // 
            this.btnmodauto.Location = new System.Drawing.Point(257, 266);
            this.btnmodauto.Name = "btnmodauto";
            this.btnmodauto.Size = new System.Drawing.Size(107, 23);
            this.btnmodauto.TabIndex = 15;
            this.btnmodauto.Text = "MODIFICACION";
            this.btnmodauto.UseVisualStyleBackColor = true;
            this.btnmodauto.Click += new System.EventHandler(this.btnmodauto_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(231, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Modelo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(232, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Precio";
            // 
            // txtmodelo
            // 
            this.txtmodelo.Location = new System.Drawing.Point(273, 28);
            this.txtmodelo.Name = "txtmodelo";
            this.txtmodelo.Size = new System.Drawing.Size(100, 20);
            this.txtmodelo.TabIndex = 18;
            // 
            // txtprecio
            // 
            this.txtprecio.Location = new System.Drawing.Point(273, 54);
            this.txtprecio.Name = "txtprecio";
            this.txtprecio.Size = new System.Drawing.Size(100, 20);
            this.txtprecio.TabIndex = 19;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtmarca);
            this.groupBox1.Controls.Add(this.txtaño);
            this.groupBox1.Controls.Add(this.txtpatente);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtprecio);
            this.groupBox1.Controls.Add(this.txtmodelo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnmodauto);
            this.groupBox1.Controls.Add(this.btnbajaauto);
            this.groupBox1.Controls.Add(this.btnaltaAuto);
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Location = new System.Drawing.Point(454, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(420, 311);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Autos";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnmodcli);
            this.groupBox2.Controls.Add(this.btnbajacli);
            this.groupBox2.Controls.Add(this.btnaltacli);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtapellido);
            this.groupBox2.Controls.Add(this.txtnombre);
            this.groupBox2.Controls.Add(this.txtdni);
            this.groupBox2.Controls.Add(this.GrillaCliente);
            this.groupBox2.Location = new System.Drawing.Point(20, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(417, 310);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Clientes";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridView2);
            this.groupBox3.Location = new System.Drawing.Point(20, 325);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(687, 203);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ventas";
            // 
            // bncomprar
            // 
            this.bncomprar.Location = new System.Drawing.Point(727, 339);
            this.bncomprar.Name = "bncomprar";
            this.bncomprar.Size = new System.Drawing.Size(75, 23);
            this.bncomprar.TabIndex = 13;
            this.bncomprar.Text = "Vender";
            this.bncomprar.UseVisualStyleBackColor = true;
            this.bncomprar.Click += new System.EventHandler(this.bncomprar_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(58, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Patente";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(61, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "año";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(61, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Marca";
            // 
            // txtpatente
            // 
            this.txtpatente.Location = new System.Drawing.Point(103, 24);
            this.txtpatente.Name = "txtpatente";
            this.txtpatente.Size = new System.Drawing.Size(100, 20);
            this.txtpatente.TabIndex = 23;
            // 
            // txtaño
            // 
            this.txtaño.Location = new System.Drawing.Point(103, 51);
            this.txtaño.Name = "txtaño";
            this.txtaño.Size = new System.Drawing.Size(100, 20);
            this.txtaño.TabIndex = 24;
            // 
            // txtmarca
            // 
            this.txtmarca.Location = new System.Drawing.Point(103, 80);
            this.txtmarca.Name = "txtmarca";
            this.txtmarca.Size = new System.Drawing.Size(100, 20);
            this.txtmarca.TabIndex = 25;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 529);
            this.Controls.Add(this.bncomprar);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GrillaCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView GrillaCliente;
        private System.Windows.Forms.TextBox txtdni;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.TextBox txtapellido;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnaltacli;
        private System.Windows.Forms.Button btnbajacli;
        private System.Windows.Forms.Button btnmodcli;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnaltaAuto;
        private System.Windows.Forms.Button btnbajaauto;
        private System.Windows.Forms.Button btnmodauto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtmodelo;
        private System.Windows.Forms.TextBox txtprecio;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtmarca;
        private System.Windows.Forms.TextBox txtaño;
        private System.Windows.Forms.TextBox txtpatente;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button bncomprar;
    }
}

