﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Negocio;


namespace Concesionaria
{
    public partial class Form1 : Form
    {



        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        
            enlazarCliente(); 

        }


        private void enlazarcompra()
        {
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = Negocio.Cliente.Compralistar();
        }
        private void enlazarCliente ()
        {

            GrillaCliente.DataSource = null;
            GrillaCliente.DataSource = Negocio.Cliente.clientelistar();

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Negocio.Auto.autolistar();

            //dataGridView2.DataSource = null;
            //dataGridView1.DataSource = Negocio.Cliente.Compralistar();

        }

        private void btnaltacli_Click(object sender, EventArgs e)
        {
           int r= Negocio.Cliente.InsertarCliente(txtdni.Text, txtnombre.Text, txtapellido.Text);
        if (r==1)
            {
                MessageBox.Show("alta ok");
                enlazarCliente();
            }
        else
            {
                MessageBox.Show("Error");
            }
        }

        private void btnmodcli_Click(object sender, EventArgs e)
        {
            Cliente cliente = (Cliente)GrillaCliente.CurrentRow.DataBoundItem;

            string m = cliente.Id.ToString();

            int r = Negocio.Cliente.UpdateCliente(m, txtdni.Text, txtnombre.Text, txtapellido.Text);
            if (r == 1)
            {
                MessageBox.Show("MOD ok");
                enlazarCliente();
            }
            else
            {
                MessageBox.Show("Error");
            }
        }

        private void btnbajacli_Click(object sender, EventArgs e)
        {
            Cliente cliente = (Cliente)GrillaCliente.CurrentRow.DataBoundItem;

            int r = Negocio.Cliente.DeleteCliente(cliente.Id);
            if (r == 1)
            {
                MessageBox.Show("Delete ok");
                enlazarCliente();
            }
            else
            {
                MessageBox.Show("Error");
            }

        }

        private void btnaltaAuto_Click(object sender, EventArgs e)
        {
            int r = Negocio.Auto.Insertarauto(txtpatente.Text, txtaño.Text, txtmarca.Text, txtmodelo.Text, txtprecio.Text);
            if (r == 1)
            {
                MessageBox.Show("alta ok");
                enlazarCliente();
            }
            else
            {
                MessageBox.Show("Error");
            }
        }

        private void btnmodauto_Click(object sender, EventArgs e)
        {
            Auto auto = (Auto)dataGridView1.CurrentRow.DataBoundItem;

            string m = auto.Id.ToString();
            int r = Negocio.Auto.updateauto(m,txtpatente.Text, txtaño.Text, txtmarca.Text, txtmodelo.Text, txtprecio.Text);
            if (r == 1)
            {
                MessageBox.Show("alta ok");
                enlazarCliente();
            }
            else
            {
                MessageBox.Show("Error");
            }


        }

        private void btnbajaauto_Click(object sender, EventArgs e)
        {

            Auto auto = (Auto)dataGridView1.CurrentRow.DataBoundItem;

            string m = auto.Id.ToString();
            int r = Negocio.Auto.deleteauto(m);
            if (r == 1)
            {
                MessageBox.Show("baja ok");
                enlazarCliente();
            }
            else
            {
                MessageBox.Show("Error");
            }



        }

        private void bncomprar_Click(object sender, EventArgs e)
        {

            Auto auto = (Auto)dataGridView1.CurrentRow.DataBoundItem;
            Cliente cliente = (Cliente)GrillaCliente.CurrentRow.DataBoundItem;



            string m = cliente.Id.ToString();
            string f = auto.Id.ToString();



           

            int r = Negocio.Cliente.Comprar(m,f, auto.Patente, cliente.Nombre  );
            if (r == 1)
            {
                MessageBox.Show("alta ok");
                enlazarcompra();
            }
            else
            {
                MessageBox.Show("Error");
                enlazarcompra();
            }

        }
    }
    }
